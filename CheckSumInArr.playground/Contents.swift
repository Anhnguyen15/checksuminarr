import UIKit

var arr = [1, 2, 3, 4, 5, -6, -9, 0, 10, 15, -20, 11, -2, 4, 5, 7]

let target = 8;

var listCouples: [(Int, Int)] = []

func isAdded(a: Int, b: Int) -> Bool {
    return listCouples.contains { (x, y) -> Bool in
        return (x, y) == (a, b) || (x, y) == (b, a)
    }
}

for i in 0...arr.count - 1
{
    for j in 0...arr.count - 1
    {
        if (i != j && arr[i] + arr[j] == target)
        {
            if (!isAdded(a: arr[i], b: arr[j]))
            {
                listCouples.append((arr[i], arr[j]))
            }
        }
    }
}

print("list result:")

for r in listCouples {
    print("[\(r.0), \(r.1)];")
}
